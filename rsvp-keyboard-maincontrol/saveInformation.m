%% saveInformation
% saveInformation routine saves the session information to the recording
% folder. Parameters folder is also saved as a compressed file.
RSVPKeyboardTaskObject.saveTaskHistory([dataFolderName '\' recordingFolder]);
rejectSequenceInfo=processingStruct.featureExtraction.rejectSequenceInfo;
save([dataFolderName '\' recordingFolder '\taskHistory.mat'],'rejectSequenceInfo','-append');
zip([dataFolderName '\' recordingFolder '\Parameters.zip'],'*','Parameters');
