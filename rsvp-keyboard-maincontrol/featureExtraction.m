%% function results = featureExtraction(dataBufferObject,triggerPartitionerStruct,featureExtractionStruct)
% Extracts the feature(s) from the data.
%   Inputs : dataBufferObject : an object that stores  data
%            triggerPartitionerStruct :
%            featureExtractionStruct : a struct for feature extraction
%
%   Outputs : results : a struct with following properties :
%             scores :
%             trialLabels : the label for each trial
%             completedSequenceCount : number of completed sequence
%             decideNextflag : a flag that can tell whether
%                              completedSequenceCount >0
%%
function [results,scoreStruct] = featureExtraction(...
                                                                 dataBufferObject,...
                                                                 cleanDataBufferObject,...
                                                                 artifactInfoBufferObject,...
                                                                 triggerPartitionerStruct,...
                                                                 featureExtractionStruct,...
                                                                 useArtifactFilteredData,...
                                                                 artifactFilteringEnabled,...
                                                                 channelStateChanged,...
                                                                 availableChannels,...
                                                                 RSVPKeyboardParams,...
                                                                 calibrationDataStruct...
                                                             )
                             
    % If there are any available channels, then perform the feature extraction
    if any(availableChannels)
        
        scoreStruct = [];
        if(featureExtractionStruct.sessionID == 1)
            results.scores = [];
            results.trialLabels = [];
        else
            if(featureExtractionStruct.completedSequenceCount > 0 && featureExtractionStruct.rejectSequenceFlag == 0)
                if useArtifactFilteredData && artifactFilteringEnabled
                    trialData=zeros(triggerPartitionerStruct.windowLengthinSamples,cleanDataBufferObject.numberofChannels,length(featureExtractionStruct.trialSampleTimeIndices));
                    for(trialIndex=1:length(featureExtractionStruct.trialSampleTimeIndices))
                        trialData(:,:,trialIndex) = cleanDataBufferObject.getOrderedData(featureExtractionStruct.trialSampleTimeIndices(trialIndex),featureExtractionStruct.trialSampleTimeIndices(trialIndex)+triggerPartitionerStruct.windowLengthinSamples-1);
                    end
                else  %% means if useArtifactFilteredData=0 and we want to use dataBuffer
                    trialData=zeros(triggerPartitionerStruct.windowLengthinSamples,dataBufferObject.numberofChannels,length(featureExtractionStruct.trialSampleTimeIndices));
                    for(trialIndex=1:length(featureExtractionStruct.trialSampleTimeIndices))
                        trialData(:,:,trialIndex) = dataBufferObject.getOrderedData(featureExtractionStruct.trialSampleTimeIndices(trialIndex),featureExtractionStruct.trialSampleTimeIndices(trialIndex)+triggerPartitionerStruct.windowLengthinSamples-1);
                    end
                end
                
                %% In the event of a channel state change, retrain the classifier with data from the available channels.
                % Check if the channel state has changed, and if there is calibration data available.
                if and(channelStateChanged,~isempty(calibrationDataStruct))
                    
                    % Calculate the scores for the calibration data using the available channels.
                    crossValidationObject = crossValidation(RSVPKeyboardParams.CrossValidation);
                    calibrationScores = crossValidationObject.apply(featureExtractionStruct.Flow,calibrationDataStruct.trialData(:,availableChannels,:),calibrationDataStruct.trialTargetness);
                    
                    % Determine the target and nontarget scores.
                    targetScores = calibrationScores(calibrationDataStruct.trialTargetness == 1);
                    nontargetScores = calibrationScores(calibrationDataStruct.trialTargetness == 0);
                    
                    % Update the score struct probability densities.
                    scoreStruct.conditionalpdf4targetKDE = kde1d(targetScores);
                    scoreStruct.conditionalpdf4nontargetKDE = kde1d(nontargetScores);
                    scoreStruct.probThresholdTarget = scoreThreshold(targetScores,scoreStruct.conditionalpdf4targetKDE.kernelWidth,0.99);
                    scoreStruct.probThresholdNontarget = scoreThreshold(nontargetScores,scoreStruct.conditionalpdf4nontargetKDE.kernelWidth,0.99);
                    
                    % Retrain the classifier.
                    featureExtractionStruct.Flow.learn(calibrationDataStruct.trialData(:,availableChannels,:),calibrationDataStruct.trialTargetness);
                    
                end
                
                % Update the resultant scores and trial labels.
                results.scores = featureExtractionStruct.Flow.operate(trialData(:,availableChannels,:));
                results.trialLabels = featureExtractionStruct.trialLabels;
                %%
                
            else
                results.scores=[];
                results.trialLabels=[];
                results.duration=0;
            end
            
        end
        
        % if(featureExtractionStruct.completedSequenceCount>0 || featureExtractionStruct.rejectSequenceFlag==1)
        if(featureExtractionStruct.completedSequenceCount > 0)
            results.completedSequenceCount=featureExtractionStruct.completedSequenceCount;
            results.decideNextFlag=true;
            results.duration=(featureExtractionStruct.trialSampleTimeIndices(length(featureExtractionStruct.trialSampleTimeIndices))-featureExtractionStruct.trialSampleTimeIndices(1)+triggerPartitionerStruct.windowLengthinSamples)/featureExtractionStruct.fs;
        else
            results.decideNextFlag=false;
        end
        
    end
    
end