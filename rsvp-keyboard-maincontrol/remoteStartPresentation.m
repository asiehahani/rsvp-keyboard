%% remoteStartPresentation
%   is a script that creates main2presentation TCPIP object and calls
%   the decisionMaker function for making decison with featureExtraction
%   result.
%
%   See also sender2receiverCommInitialize
%   generateArtificialTriggers
%%

dos('startPresentation.bat &');

[~,main2presentationCommObjectStruct,BCIPacketStruct] = sender2receiverCommInitialize('main','presentation',false,RSVPKeyboardParams.IP_presentation,RSVPKeyboardParams.port_mainAndPresentation);

results.decideNextFlag=1;
results.trialLabels=[];
results.completedSequenceCount=0;
results.duration=0;
[~,decision] = decisionMaker(results,RSVPKeyboardTaskObject,main2presentationCommObjectStruct,BCIPacketStruct);

if(strcmp(amplifierStruct.DAQType,'noAmp'))
    generateArtificialTriggers(amplifierStruct,decision);
end