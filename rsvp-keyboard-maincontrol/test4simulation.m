addpath(genpath('.'));

sessionType='CopyPhraseTask';
load('testCalibrationFile');
RSVPKeyboardParameters;

imageStructs=xls2Structs('imageList.xls');  %OK

simulationResults=simulateTypingPerformance(scoreStruct,imageStructs,sessionType,RSVPKeyboardParams);