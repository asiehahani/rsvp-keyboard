%% RSVPKeyboard
% RSVPKeyboard.m starts the RSVP Keyboard, BCI typing system. It initializes the data acquisition
% using gTec, starts the language model server (btlm) and starts a separate MATLAB session for the
% presentation. All of the signal processing and machine learning modules are handled in this
% routine. Presentation information transmitted to the presentation MATLAB. The contextual
% probabilities are updated and acquired by communicating with the language model server. See
% \Documents\RSVPKeyboardOperationManual.pdf for more detailed information on the operation of
% RSVPKeyboard system.
%%

clear;
clc;

%% Initialization of parameters
% Initializes the parameters, queries the user input for subject and session and sets up the
% recording folders and files.
global BCIframeworkDir

BCIframeworkDir='.';
addpath(genpath('.'));

RSVPKeyboardParameters

projectID='CSL_RSVPKeyboard';
subjectID=getSubjectInformation;
[sessionID,sessionName]=getSessionInformation;

setPathandFilenames

%% Initialization of data acquisition
% Initializes the data acquisition device, applies a trigger test and starts signal quality check
% GUI.
initializeDAQ
if(success==0),return,end

DAQSignalCheck
if ~continueMainBCIFlag
    [success]=stopAmps(amplifierStruct);
    [success]=closeAmps(amplifierStruct);
    return;
end

availableChannels = true(1,length(amplifierStruct.channelNames));
channelStateChanged = false;

%% Initialization signal processing and machine learning
% Initializes the buffers, filters, feature extraction, language model and decision maker.
initializeSignalProcessing
initializeMachineLearning
artifactFilteringParameters
clear artifactFiltering

decisionLoop = true;
while decisionLoop
    
    [selection,~] = listdlg(...
                            'PromptString','Select which modules you would like to run:',...
                            'ListString',RSVPKeyboardParams.modules...
                           );
    
    if isempty(selection)
        decisionLoop = false;
        showPresentation = false;
        showGUI = false;
    else
        showPresentation = any(selection == 1);
        showGUI = any(selection == 2);
    end
    
    %% Start GUI MATLAB
    remoteGUI = [];
    displayMode = 0;
    if showGUI;
        remoteGUI = RemoteSignalMonitorGUI();
        remoteGUI.start(RSVPKeyboardParams.GUI.autoLaunch,RSVPKeyboardParams.IP_main,RSVPKeyboardParams.IP_GUI,RSVPKeyboardParams.port_mainAndGUI);
        remoteGUI.setChannelNames(amplifierStruct.channelNames);
        remoteGUI.setSampleRate(amplifierStruct.fs);
    end;

    %% Start presentation MATLAB
    if showPresentation;
        remoteStartPresentation;
    end;
    
    %% Start recording
    if or(showGUI,showPresentation)
        startAcquisition;
    end

    %% Start main loop
    while or(showGUI,showPresentation)

        %% Fetch the data
        fetchData;


        %% SignalProcessing
        % Reads the data and trigger from the amplifer applies the frontEnd filter
        % to the data and stores the data in the dataBuferObject and the trigger in
        % triggerBufferObject.
        [~,filteredData] = signalProcessing(...
                                               rawData,...
                                               triggerData,...
                                               dataBufferObject,...
                                               triggerBufferObject,...
                                               frontendFilter...
                                           );

        %% Trigger Control and Partitioning
        % Based on the triggers collected in the buffer, checks the completion of the sequences and
        % partitions the sequence into trials accordingly.
        [processingStruct.triggerPartitioner.firstUnprocessedTimeIndex,processingStruct.featureExtraction.completedSequenceCount,processingStruct.featureExtraction.trialSampleTimeIndices,processingStruct.featureExtraction.trialTargetness,processingStruct.featureExtraction.trialLabels]=triggerDecoder(triggerBufferObject,processingStruct.triggerPartitioner);

        %% Artifacts
        % Based on the setting detects the artifacts and tries to remove them from
        % the signal.
        [processingStruct.featureExtraction,newAvailableChannels] = artifactFiltering(...
                                                                                        amplifierStruct,...
                                                                                        dataBufferObject,...
                                                                                        cleanDataBufferObject,...
                                                                                        artifactInfoBufferObject,...
                                                                                        triggerBufferObject,...
                                                                                        fs,...
                                                                                        processingStruct.triggerPartitioner,...
                                                                                        processingStruct.featureExtraction,...
                                                                                        numberOfChannels,...
                                                                                        artifactFilteringParams,...
                                                                                        RSVPKeyboardParams.artifactFiltering.rejectSequence,...
                                                                                        sessionID,...
                                                                                        RSVPKeyboardParams.artifactFiltering.enabled,...
                                                                                        remoteGUI...
                                                                                    );

        % Check if the state of the channels has changed.
        channelStateChanged = any(newAvailableChannels ~= availableChannels);
        if channelStateChanged
            
            availableChannels = newAvailableChannels;
            
        end
                                                                                
        %% Attention Monitoring
        % Based on the setting detects the attention during experiment.
%         attentionMonitoringStruct = attentionMonitoring(...
%                                                             RSVPKeyboardParams.attentionMonitoring.enabled,...
%                                                             RSVPKeyboardParams.attentionMonitoring.useArtifactFilteringData,...
%                                                             RSVPKeyboardParams.attentionMonitoring.processWindowLength,...
%                                                             fs,...
%                                                             dataBufferObject,...
%                                                             cleanDataBufferObject,...
%                                                             RSVPKeyboardParams.artifactFiltering.enabled,...
%                                                             amplifierStruct.channelNames...
%                                                        );

        %% Feature Extraction
        % Extracts the feature(s) corresponding to trials from the filtered EEG.
        [results,scoreStruct] = featureExtraction(...
                                                                dataBufferObject,...
                                                                cleanDataBufferObject,...
                                                                artifactInfoBufferObject,...
                                                                processingStruct.triggerPartitioner,...
                                                                processingStruct.featureExtraction,...
                                                                RSVPKeyboardParams.artifactFiltering.useArtifactFilteredData,...
                                                                RSVPKeyboardParams.artifactFiltering.enabled,...
                                                                channelStateChanged,...
                                                                availableChannels,...
                                                                RSVPKeyboardParams,...
                                                                calibrationDataStruct...
                                                            );
        
        if ~isempty(scoreStruct)
            RSVPKeyboardTaskObject.decisionObj.scoreStruct = scoreStruct;
        end
        
        if showPresentation
            
            %% Decision making
            % Makes decision based on the extracted features and language model. The decisions and the next
            % trials to show is sent to the presentation MATLAB via TCP/IP.
            [showPresentation,decision] = decisionMaker(...
                                                            results,...
                                                            RSVPKeyboardTaskObject,...
                                                            main2presentationCommObjectStruct,...
                                                            BCIPacketStruct...
                                                        );

            %% Generate artificial triggers
            % In no amplifier mode ('noAmp'), triggers are generated artificially if no input file is selected.
            if(strcmp(amplifierStruct.DAQType,'noAmp'))

                generateArtificialTriggers(amplifierStruct,decision);

            end
                                                    
        end

        %% Check packets from presentation
        % Gets feedback from the presentation.
        if showPresentation

            inPacket = receiveBCIPacket(main2presentationCommObjectStruct.main2presentationCommObject,BCIPacketStruct);

            switch inPacket.header

                case BCIPacketStruct.HDR.STOP
                    showPresentation = false;

            end

        end

        %% Communication with GUI
        if showGUI

            % Gets feedback from the GUI.
            newDisplayMode = remoteGUI.getDisplayMode(1);

            if displayMode ~= newDisplayMode

                displayMode = newDisplayMode;

                % Initialise new display mode properties.
                switch displayMode;

                    case 0
                        showGUI = false;

                    case {RSVPKeyboardParams.GUI.RAW_DATA,RSVPKeyboardParams.GUI.FILTERED_DATA}
                        remoteGUI.setChannelNames(amplifierStruct.channelNames);

                    case RSVPKeyboardParams.GUI.AM_DATA
                        remoteGUI.setChannelNames({'Theta Power','Alpha Power','Lateral Eye Movement','Eye Blink Rate'});
                end

            end

            % Sends data to the GUI, based on the display mode.
            if ~isempty(rawData)

                switch displayMode;

                    case RSVPKeyboardParams.GUI.RAW_DATA
                        remoteGUI.addData(rawData);

                    case RSVPKeyboardParams.GUI.FILTERED_DATA
                        remoteGUI.addData(filteredData);

                    case RSVPKeyboardParams.GUI.AM_DATA

                        if attentionMonitoringStruct.show
                            % Prepare data packet.
                            AMData = [transpose(attentionMonitoringStruct.thetaPower),transpose(attentionMonitoringStruct.alphaPower),transpose(attentionMonitoringStruct.lateralEyeMovement),transpose(attentionMonitoringStruct.eyeBlinkRate)];
                            remoteGUI.addData(AMData);
                        end

                end

            end

        end

        pause(0.01);
    end
    
    % Stop, but do not destroy, the DAQ(s).
    switch amplifierStruct.DAQType
        
        case 'gUSBAmp'
            stopAmps(amplifierStruct);
            
        case 'noAmp'
            amplifierStruct.awaitingTriggers = [];
    end
    
    if any(selection == 1)
        
        outPacket.header = BCIPacketStruct.HDR.STOP;
        outPacket.data = [];
        sendBCIPacket(main2presentationCommObjectStruct.main2presentationCommObject,BCIPacketStruct,outPacket);
        
    end
    
    if any(selection == 2)
        
        remoteGUI.stop();
        
    end

end

%% Save information and quit
saveInformation;
quitDAQ(amplifierStruct);