%% Publishes all .m files in the directory in which this script is run

files = getAllFiles([],'.m');
for i = 1:numel(files); publish(files{i},'evalCode',false); end;