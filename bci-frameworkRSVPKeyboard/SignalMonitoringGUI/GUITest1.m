clear;
clc;

GUIObj = SignalMonitorGUI();
GUIObj.setChannelNames({'A','B','C','D'});
GUIObj.setSampleRate(10);
GUIObj.start();

while GUIObj.isStarted()
    
    switch GUIObj.getDisplayMode();

        case 2
            % Send data packet.
            data = (rand(1,3) - 0.5) / 10;
            data = [data,sin(GetSecs() * 5) / 10];
            GUIObj.addData(data);

    end
    
    pause(0.01);

end

GUIObj.stop();