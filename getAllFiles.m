%% function [fileList] = getAllFiles( directoryName, fileExtension )
% Gets the names of all the files in the directory specified. If no directory is specified, it get the files of the current directory.
%
% Modified from http://stackoverflow.com/a/2654459
%%

function [fileList] = getAllFiles( directoryName, fileExtension )

    if nargin == 0
        
        directoryName = [];
        fileExtension = [];
        
    elseif nargin == 1
        
        fileExtension = [];
        
    end
    
    if isempty(directoryName)
        
        directoryName = pwd();
        
    end
    
    directoryData = dir(directoryName);
    subdirectoryIndices = [directoryData.isdir];
    fileList = {directoryData(~subdirectoryIndices).name};
    fileExtensionIndices = false(length(fileList),1);
    
    for i = 1:length(fileList)
        
        fileList{i} = fullfile(directoryName,fileList{i});
        
        if ~isempty(fileExtension)
            
            [~,~,extension] = fileparts(fileList{i});
            
            if strcmp(extension,fileExtension)
                
                fileExtensionIndices(i) = true;
                
            end
            
        end
        
    end
    
    if ~isempty(fileExtension)
        
        fileList = fileList(fileExtensionIndices);
        
    end
    
    subdirectoryNames = {directoryData(subdirectoryIndices).name};
    validIndex = ~ismember(subdirectoryNames,{'.','..'});
    
    for iDir = find(validIndex)
        
        nextDirectory = fullfile(directoryName,subdirectoryNames{iDir});
        fileList = [fileList,getAllFiles(nextDirectory,fileExtension)];
        
    end

end